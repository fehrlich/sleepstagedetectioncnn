import setuptools

from sleepClassificationCNN import __version__

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="sleepClassificationCNN",
    version=__version__,
    author="fehrlich",
    author_email="",
    description="CNN Model to recognize sleep stages",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(exclude=["tests"]),
    install_requires = ['pyPhases'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
