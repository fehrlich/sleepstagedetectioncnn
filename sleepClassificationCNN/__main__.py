from pyPhases.Project import Project

# from pyPhases.publisher.DotScience import DotScience
from pyPhases.storage.FileStorage import FileStorage
from pyPhases.exporter.ObjectExporter import ObjectExporter
from pyPhases.exporter.KerasExporter import KerasExporter
# from publisher.MLFlow import MLFlow
# from publisher.Tensorboard import Tensorboard
from phases.GetEdfDataForML import GetEdfDataForML
from phases.EDF4SleepStages import EDF4SleepStages
from phases.SleepPhaseDetectionModel import SleepPhaseDetectionModel
from phases.SleepPhaseDetectionModelEval import SleepPhaseDetectionModelEval
from phases.FilterEDFData import FilterEDFData

import sys
import numpy
# import tensorflow

numpy.random.seed(0)

# Project setup
project = Project()
project.debug()
project.name = 'sleepClassificationCNN'
project.namespace = 'tud.ibmt'

project.setClasses([
    "Wach",
    "Leichter Schlaf",
    "Tiefer Schlaf",
    "REM",
])

#used publisher
# project.registerPublisher(DotScience({
#     "username": "fehrlich",
#     "apikey": "PGKZXYKQWU667DIU7JSI5XMN4BSGX7JC2O2RN7PLHEXJ7DOWUF3Q====",
#     "projectname": "tud.imbt.teleschlafmedizin.test",
#     "url": "https://cloud.dotscience.com",
#     "modelName": "sleepClassificationCNN",
#     "modelType": tensorflow,
# }))
# project.registerPublisher(MLFlow())
# project.registerPublisher(Tensorboard())

#used exporter
project.registerExporter(ObjectExporter())
project.registerExporter(KerasExporter())

# used storage engine
project.addStorage(FileStorage({
    "basePath": "data/"
}))

# stages
project.addStage("gather")
project.addStage("prepare")
project.addStage("train")
project.addStage("evaluate")

# add phases
project.addPhase(GetEdfDataForML(), "gather", "get EDF Data and split them into training/validation/evaluation set")
project.addPhase(EDF4SleepStages(), "prepare", "Prepare EDF Data for sleep stage recognition")
project.addPhase(SleepPhaseDetectionModel(), "train", "Create Model for sleep stage recognition")
project.addPhase(SleepPhaseDetectionModelEval(), "evaluate", "Create Model for sleep stage recognition")

# run all phases or a specific phase if there is an argument
if(len(sys.argv) > 1):
    project.run(sys.argv[1])
else:
    project.run()
