from pyPhases import Phase
from pyPhases.publisher import Publisher

import os
from mlflow import log_metric, log_param, log_artifact
from distutils.dir_util import copy_tree
import tensorflow
import datetime

class Tensorboard(Publisher):
    connected = False

    def before(self, phase: Phase):
        MODEL_DIR = "./dist/model"
        if (Tensorboard.connected == False):
            if not os.path.isdir(MODEL_DIR):
                os.makedirs(MODEL_DIR)
            log_dir = "./dist/logs/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
            tensorboard_callback = tensorflow.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

    def publish(self, phase: Phase):
        MODEL_DIR = "./dist/model"


