from pyPhases import Phase
from pyPhases.publisher import Publisher

import os
from mlflow import log_metric, log_param, log_artifact
from distutils.dir_util import copy_tree

class MLFlow(Publisher):
    connected = False

    def publish(self, phase: Phase):
        if (MLFlow.connected == False):
            import mlflow.keras
            MLFlow.connected = True

        MODEL_DIR = "./dist/model"

        if (phase.config):
            for (value, name) in enumerate(phase.config):
                value = phase.config[name]
                self.log("add parameter: %s = %s"%(name, str(value)))
                log_param(name, value)
        if (phase.metrics):

            for (value, name) in enumerate(phase.metrics):
                value = phase.metrics[name]
                self.log("add metric: %s = %s"%(name, str(value)))
                log_metric(name, value)

        if (phase.model):
            if not os.path.isdir(MODEL_DIR):
                os.makedirs(MODEL_DIR)

            export_path = os.path.join(MODEL_DIR)
            copy_tree("mlruns", export_path)
