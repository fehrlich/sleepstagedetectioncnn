import mne
from pyPhases import Phase

from .data.EDFData import EDFData
from mne.io.fiff.raw import Raw
from .EDF4SleepStages import EDF4SleepStages


class FilterEDFData(Phase):
    """get EDF Data and split them into training/validation/evaluation set

    on execution the method main is called
    you can use the phase attributes for better artifact handling
    self.config all configuration input should be stored here (p.e. self.config['epochLength'] = 30.)
    self.metrics the evaluated metrics like 'loss' or 'accuracy' (p.e. self.metrics['accuracy'] = 0.9)
    self.summary
    """

    trainingData: Raw

    def main(self):
        trainingData = EDFData(
            "data/",
            "https://physionet.org/files/sleep-edfx/1.0.0/sleep-telemetry/")

        # train data composition
        trainingData.fromUrl("ST7011J0-PSG.edf", "ST7011JP-Hypnogram.edf")

        labels = self.project.classesMap
        preparePhase = EDF4SleepStages()
        preparePhase.data = trainingData.raw
        epochs = preparePhase.prepareData(labels)

        self.showEpochs(epochs)


        x_train, y_train = self.segRawData(labels)


    def showRaw(self, edfData: EDFData):
        rawData = edfData.raw
        rawData.plot(show_options=True, block=True)

    def showEpochs(self, epochs):
        epochs.plot(block=True)
