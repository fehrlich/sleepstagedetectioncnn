from urllib.request import urlretrieve
from mne.io import read_raw_edf
from mne import read_annotations
import os.path
# import pyedflib


class EDFData():
    def __init__(self, filePath="data/", urlPath= ""):
        self.filePath = filePath
        self.urlPath = urlPath
        self.rawArray = []
        self.rawMetaData = []
        self.getMetaDataCallable = None

    def fromUrl(self, relUrlEdf : str, relUrlAnnotation : str = None, force=False):
        """ Description Loads a File from an url into a local file and calls addDataFromFile afterwards
        """
        annotFileName = None

        url = self.urlPath + relUrlEdf
        fileName = url.split('/')[-1]
        if force or not os.path.isfile(fileName):
            urlretrieve(url, self.filePath + fileName)
        if(relUrlAnnotation):
            relUrlAnnotation = self.urlPath + relUrlAnnotation
            annotFileName = relUrlAnnotation.split('/')[-1]
            if force or not os.path.isfile(annotFileName):
                urlretrieve(relUrlAnnotation, self.filePath + annotFileName)
        print("FILES: " + fileName + "..." + annotFileName)
        self.addDataFromFile(fileName, annotFileName)

    # get data from EDF Files and add optional Notations, returns MNE.Raw
    # (https://mne.tools/stable/generated/mne.io.Raw.html#mne.io.Raw)
    def addDataFromFile(self, relFilePath, relAnnotationPath=None):

        dataFilePath = self.filePath
        dataAnnotationPath = dataFilePath

        filePath = dataFilePath+relFilePath
        # self.rawMetaData = pyedflib.EdfReader(filePath)
        dataSet = read_raw_edf(filePath)

        if(relAnnotationPath):
            fileAnnotationPath = dataAnnotationPath+relAnnotationPath
            annoationStages = read_annotations(fileAnnotationPath)
            dataSet.set_annotations(annoationStages)

        metaData = {}
        if(self.getMetaDataCallable):
            metaData = self.getMetaDataCallable(self)
        self._append(dataSet, metaData)

        return dataSet

    def _append(self, data, metaData = {}):
            self.rawArray.append((data, metaData))

    # Files Array containting [file, annotationFiles?]
    def addDataFromFiles(self, filesArray):
        for file, annotationFile in filesArray:
            self.addDataFromFile(file, annotationFile)

    def getData(self, id):
        return self.rawArray

