import mne
from pyPhases import Phase

from .data.EDFData import EDFData
from mne.io.fiff.raw import Raw
from .EDF4SleepStages import EDF4SleepStages
from mne.io.base import BaseRaw


class FilterEDFData(Phase):
    """get EDF Data and split them into training/validation/evaluation set

    on execution the method main is called
    you can use the phase attributes for better artifact handling
    self.config all configuration input should be stored here (p.e. self.config['epochLength'] = 30.)
    self.metrics the evaluated metrics like 'loss' or 'accuracy' (p.e. self.metrics['accuracy'] = 0.9)
    self.summary
    """

    trainingData: Raw

    def prepareData(self, labels):
        dataSet = self.data
        self.config["numClasses"] = len(labels)
        channelTypeMapping = self.channelTypeMapping
        annotationEventIds = self.annotationEventIds

        self.config["annotationDuration"] = 30. - 1. / dataSet.info['sfreq']
        chunkDuration = self.config["annotationChunkDuration"]
        tmin = self.config["annotationBefore"]
        tmax = self.config["annotationDuration"]

        dataSet.set_channel_types(channelTypeMapping)

        annotationEvents, _ = mne.events_from_annotations(
            dataSet, event_id=annotationEventIds, chunk_duration=chunkDuration)

        epochs = mne.Epochs(raw=dataSet,
                            # picks=["eeg", "eog", "emg"],
                            events=annotationEvents,
                            event_id=labels,
                            tmin=tmin,
                            tmax=tmax,
                            baseline=None)

        return epochs

    def main(self):
        labels = self.project.classesMap
        self.data = self.project.getData("trainingRaw", BaseRaw)
        epochs_train = self.prepareData(labels)

        self.data = self.project.getData("validationRaw", BaseRaw)
        epochs_validation = self.prepareData(labels)

        self.data = self.project.getData("evaluationRaw", BaseRaw)
        epochs_eval = self.prepareData(labels)

