from pyPhases import Phase
import os
import keras
import numpy as np
import pandas
import seaborn as sns

from keras.models import load_model, Sequential
from keras.layers import Dense, Dropout, Flatten, Reshape, GlobalAveragePooling1D
from keras.layers import MaxPooling2D, Conv1D, MaxPooling1D
from keras.utils import np_utils, plot_model


from matplotlib import pyplot as plt

from sklearn import metrics
from sklearn.metrics import classification_report

class SleepPhaseDetectionModel(Phase):
    """Create Model for sleep stage recognition

    on execution the method main is called
    you can use the phase attributes for better artifact handling
    self.config all configuration input should be stored here (p.e. self.config['epochLength'] = 30.)
    self.metrics the evaluated metrics like 'loss' or 'accuracy' (p.e. self.metrics['accuracy'] = 0.9)
    self.summary
    """

    exportData = ["model",]

    print('keras version ', keras.__version__)
    channelSize = 4

    def __init__(self):
        self.config = {
            'batchSize': 32,
            'epochs': 100,
            'optimizer': 'adam',
            'loss': 'categorical_crossentropy',
            # 'validation_split': 0.2,
            'learningRate': 0.0001,
            'patience': 3,
            # channels: eeg, eeg, eog, emg + meta: "age", "gender"
            # slice(0, -2) for removing metadata
           'sliceChannels': slice(0,None)
        }

    def getModelInputShape(self):
        dataSize = self.project.getConfig("timePeriods")
        return (dataSize, self.channelSize)

    def defineModel(self, num_classes, TIME_PERIODS, num_sensors, input_shape):
        # 1D CNN neural network
        model_m = Sequential()

        shape = self.getModelInputShape()
        (periods, channels) = shape

        # model_m.add(
        #     Reshape(shape, input_shape=shape))
        # model_m.add(Conv1D(100, 10, activation='relu',
        #                    input_shape=(TIME_PERIODS, num_sensors)))
        # 1	1D Conv	64 × 5	-	ReLU, Stride = 3	384	64 × 999
        model_m.add(
            Conv1D(64,
                   5,
                   activation='relu',
                   strides=3,
                   input_shape=shape))
        # 2	1D Conv	128 × 5	-	ReLU, Stride = 1	24,704	128 × 997
        model_m.add(Conv1D(128, 5, activation='relu'))
        # 3	MaxPool	-	2	Stride = 2	0	128 × 498
        model_m.add(MaxPooling1D(2, strides=2))
        # # 4	Dropout	-	-	Rate = 0.2	0	128 × 498
        model_m.add(Dropout(0.2))

        # 5	1D Conv	128 × 13	-	ReLU, Stride = 1	213,120	128 × 486
        model_m.add(Conv1D(128, 5, activation='relu'))
        # 6	1D Conv	256 × 7	-	ReLU, Stride = 1	229,632	256 × 480
        model_m.add(Conv1D(256, 7, activation='relu'))
        # 7	MaxPool	-	2	Stride = 2	0	256 × 240
        model_m.add(MaxPooling1D(2, strides=2))
        # 8	1D Conv	256 × 7	-	ReLU, Stride = 1	262,272	128 × 233
        model_m.add(Conv1D(256, 7, activation='relu'))
        # 9	1D Conv	64 × 4	-	ReLU, Stride = 1	32,832	64 × 230
        model_m.add(Conv1D(64, 4, activation='relu'))
        # 10	MaxPool	-	2	Stride = 2	0	64 × 115
        model_m.add(MaxPooling1D(2, strides=2))
        # 11	1D Conv	32 × 3	-	ReLU, Stride = 1	6176	32 × 113
        model_m.add(Conv1D(32, 3, activation='relu'))
        # 12	1D Conv	64 × 6	-	ReLU, Stride = 1	12,352	64 × 108
        model_m.add(Conv1D(64, 6, activation='relu'))
        # 13	MaxPool	-	2	Stride = 2	0	64 × 54
        model_m.add(MaxPooling1D(2, strides=2))
        # 14	1D Conv	8 × 5	-	ReLU, Stride = 1	2568	8 × 50
        model_m.add(Conv1D(8, 5, activation='relu'))
        # 15	1D Conv	8 × 2	-	ReLU, Stride = 1	136	8 × 49
        model_m.add(Conv1D(8, 2, activation='relu'))
        # 16	MaxPool	-	2	Stride = 2	0	8 × 24
        model_m.add(MaxPooling1D(2, strides=2))
        # 17	Flatten	-	-	-	0	1 × 192
        model_m.add(Flatten())
        # 18	Dense	-	64	ReLU, Drop = 0.2	12,352	1 × 64
        model_m.add(Dense(num_classes, activation='relu'))
        # 19	Dense	-	nb_class	Softmax	195	1 × nb_class
        model_m.add(Dense(num_classes, activation='softmax'))

        self.model = model_m

        # plot_model(model_m, to_file="dist/model.png", show_shapes=True)
        return model_m

    def definemodel2(self):
        model_m = Sequential()

        shape = self.getModelInputShape()
        (periods, channels) = shape

        # model_m.add(
        #     Reshape(shape, input_shape=shape))
        # model_m.add(Conv1D(100, 10, activation='relu',
        #                    input_shape=(TIME_PERIODS, num_sensors)))
        # 1	1D Conv	64 × 5	-	ReLU, Stride = 3	384	64 × 999
        model_m.add(
            Conv1D(64,
                   5,
                   activation='relu',
                   strides=3,
                   input_shape=shape))

    def trainModel(self, x_train, y_train, x_val, y_val):
        model_m = self.model
        modelDir = 'dist'

        if not os.path.exists(modelDir):
            os.makedirs(modelDir)
        callbacks_list = [
            keras.callbacks.ModelCheckpoint(
                filepath=modelDir +
                '/best_model.{epoch:02d}-{val_loss:.2f}.h5',
                monitor='val_loss',
                save_best_only=True),
            keras.callbacks.EarlyStopping(monitor='accuracy', patience=self.config["patience"])
        ]

        optimizer = keras.optimizers.Adam(
            learning_rate=self.config["learningRate"])
        # optimizer = self.config["optimizer"]

        model_m.compile(loss=self.config['loss'],
                        optimizer=optimizer,
                        metrics=['accuracy'])

        # Enable validation to use ModelCheckpoint and EarlyStopping callbacks.
        model = model_m.fit(x_train,
                            y_train,
                            batch_size=self.config["batchSize"],
                            epochs=self.config["epochs"],
                            callbacks=callbacks_list,
                            # validation_split=self.config["validation_split"],
                            validation_data=(x_val, y_val),
                            verbose=2)
        self.history = model.history

        return model

    def fitDataToInputShape(self, xData):
        xData = xData.reshape(len(xData), 3000, 6)
        if self.config["sliceChannels"]:
            xData = xData[:,slice(0,None), self.config["sliceChannels"]]

        self.channelSize = len(xData[0][0])

        return xData

    def create(self, xTrain, yTrain, xValidate, yValidate, dataConfig):
        # run the fitting first, because we need some data for the model creation
        xTrain = self.fitDataToInputShape(xTrain)

        self.defineModel(dataConfig["numClasses"], dataConfig["timePeriods"],
                         dataConfig["numSensors"], dataConfig["shape"])

        xValidate = self.fitDataToInputShape(xValidate)
        self.trainModel(xTrain, yTrain, xValidate, yValidate)
        self.exportLearningCurve(self.history)

        return self.model

    def load(self, modelPath):
        self.model = load_model(modelPath)

        return self.model

    def exportLearningCurve(self, history):
        plt.figure(figsize=(6, 4))
        plt.plot(history['accuracy'], "g--", label="Accuracy of training data")
        plt.plot(history['val_accuracy'],
                 "g",
                 label="Accuracy of validation data")
        plt.plot(history['loss'], "r--", label="Loss of training data")
        plt.plot(history['val_loss'], "r", label="Loss of validation data")
        plt.title('Model Accuracy and Loss')
        plt.ylabel('Accuracy and Loss')
        plt.xlabel('Training Epoch')
        plt.ylim(0)
        plt.legend()
        plt.savefig("dist/learningcurve.svg")

    def exportConfusionMatrix(self, validations, predictions):

        matrix = metrics.confusion_matrix(validations, predictions)
        plt.figure(figsize=(6, 4))
        sns.heatmap(matrix,
                    cmap="coolwarm",
                    linecolor='white',
                    linewidths=1,
                    xticklabels=self.labels,
                    yticklabels=self.labels,
                    annot=True,
                    fmt="d")
        plt.title("Confusion Matrix")
        plt.ylabel("True Label")
        plt.xlabel("Predicted Label")
        plt.savefig("dist/confusionmatrix.svg")

    def exportClassificationReport(self, max_y_test, max_y_pred_test):
        report = classification_report(max_y_test,
                                       max_y_pred_test,
                                       output_dict=True)
        df = pandas.DataFrame(report).transpose()
        df.to_html("dist/classification_report.html")

    def evaluate(self, x_test, y_test):
        score = self.model.evaluate(x_test, y_test, verbose=0)
        self.metrics["loss"] = score[0]
        self.metrics["accuracy"] = score[1]

        y_pred_test = self.model.predict(x_test)
        # Take the class with the highest probability from the test predictions
        max_y_pred_test = np.argmax(y_pred_test, axis=1)
        max_y_test = np.argmax(y_test, axis=1)

        self.exportConfusionMatrix(max_y_test, max_y_pred_test)
        self.exportClassificationReport(max_y_test, max_y_pred_test)

        return self.model

    def main(self):
        project = self.project
        xTrain, yTrain = project.getData('trainingTransformed', np.ndarray)
        xVal, yVal = project.getData('validationTransformed', np.ndarray)
        self.labels = project.classesMap

        config =  {
            'numClasses': project.getConfig("numSensors"),
            'numMetaData': project.getConfig("numMetaData"),
            'timePeriods': project.getConfig("timePeriods"),
            'numSensors': project.getConfig("numSensors"),
            'shape': project.getConfig("shape"),
        }


        self.create(xTrain, yTrain, xVal, yVal, config)
        self.project.registerData("model", self.model)
