from pyPhases import Phase

import mne
import numpy as np
from sklearn import preprocessing
from keras.utils import np_utils
from mne.io import BaseRaw
import pandas


class EDF4SleepStages(Phase):
    """Prepare EDF Data for sleep stage recognition

    on execution the method main is called
    you can use the phase attributes for better artifact handling
    self.config all configuration input should be stored here (p.e. self.config['epochLength'] = 30.)
    self.metrics the evaluated metrics like 'loss' or 'accuracy' (p.e. self.metrics['accuracy'] = 0.9)
    self.summary
    """

    exportData = ["trainingTransformed","validationTransformed",]


    def __init__(self):
        self.channelTypeMapping = {
            'EEG Fpz-Cz': 'eeg',
            'EEG Pz-Oz': 'eeg',
            'EOG horizontal': 'eog',
            'EMG submental': 'emg',
            'Marker': 'misc',
        }
        self.droppedEpochs = 0
        self.annotationEventIds = {
            'Sleep stage W': 0,
            'Sleep stage 1': 1,
            'Sleep stage 2': 1,
            'Sleep stage 3': 2,
            'Sleep stage 4': 2,
            'Sleep stage R': 3,
        }
        self.data = None
        self.config = {
            'timePeriods': 3000,  # 30 sekunden
            'stepDistance': 100,
            'annotationChunkDuration': 30.,
            'annotationBefore': 0.,
            'numClasses': None,
            'annotationDuration': None,
            'numSensors': 4,
            'numMetaData': 2
        }
        self.config['shape'] = self.config['timePeriods'] * (self.config['numSensors'] + self.config['numMetaData']) # channels x data

    def prepareDataSet(self, labels, dataSet):
        self.config["numClasses"] = len(labels)
        channelTypeMapping = self.channelTypeMapping
        annotationEventIds = self.annotationEventIds

        self.config["annotationDuration"] = 30. - 1. / dataSet.info['sfreq']
        chunkDuration = self.config["annotationChunkDuration"]
        tmin = self.config["annotationBefore"]
        tmax = self.config["annotationDuration"]

        dataSet.set_channel_types(channelTypeMapping)

        annotationEvents, _ = mne.events_from_annotations(
            dataSet, event_id=annotationEventIds, chunk_duration=chunkDuration)

        epochs = mne.Epochs(raw=dataSet,
                            picks=["eeg", "eog", "emg"],
                            events=annotationEvents,
                            event_id=labels,
                            tmin=tmin,
                            tmax=tmax,
                            # preload=True,
                            baseline=None)
        # curLen = len(epochs)
        epochs.equalize_event_counts(labels)
        # dropped = curLen - len(epochs)
        # self.droppedEpochs += dropped
        # self.log("Dropped Epochs because of equalization: %s"%(dropped))

        return epochs

    def segmentData(self, df, time_steps, step, labelId):
        features = df.keys()
        featureCount = df.keys().size
        segments = []
        labels = []

        for i in range(0, len(df) - time_steps, step):
            subsegment = []
            for featureIndex in range(0, featureCount):
                featureName = features[featureIndex]
                value = df[featureName].values[i:i + time_steps]
                subsegment.append(value)
            # label = df.iloc[featureIndex].name[0]
            # labels.append(label)
            segments.append(subsegment)
            labels.append(labelId)

        return segments, labels

    def getWholeSegment(self, df, labelId, eventCount):
        labels = np.empty(eventCount)
        labels.fill(labelId)
        labels.astype("float32")

        return df.values, labels

    def segEpochs(self, epochsSet, labels):
        firstEpoche, __ = epochsSet[0]
        metaDataCount = 2
        featureCount = len(firstEpoche.ch_names)
        featureCount += metaDataCount

        y_train = []
        x_train = np.empty((0, featureCount), "float32")

        for epochs, metaData in epochsSet:
            for name in labels:
                labelId = labels[name]
                epoch = epochs[name]
                epoch.drop_bad()
                eventCount = len(epoch)
                dataFrame = epoch.to_data_frame()
                indexesToFill = len(dataFrame)
                for index in metaData:
                    # print("ADD " + index + ": " + str(metaData[index]))
                    # dataFrame = pandas.concat([dataFrame], keys=[metaData[index]], names=[index])
                    dataRow = np.empty(len(dataFrame))
                    dataRow.fill(metaData[index])
                    dataFrame[index] = pandas.Series(dataRow, index=dataFrame.index)
                xt, yt = self.getWholeSegment(dataFrame, labelId, eventCount)

                # xt, yt = self.segmentData(dataFrame, self.config["timePeriods"],
                #                           self.config["stepDistance"], labelId)
                x_train = np.append(x_train, xt, axis=0)
                y_train = np.append(y_train, yt, axis=0)

        x_train = x_train.reshape(-1, self.config["timePeriods"], featureCount)
        return x_train, y_train

    def transform4Keras(self, xArray, yArray):
        x_train, y_train = xArray, yArray
        num_time_periods, num_sensors = x_train.shape[1], x_train.shape[2]
        num_classes = self.config["numClasses"]
        input_shape = (num_time_periods * num_sensors)
        x_train = x_train.reshape(x_train.shape[0], input_shape)

        self.log("x_train shape: %s"%(str(x_train.shape)))
        self.log("input_shape shape: %s"%(str(input_shape)))

        y_train = np_utils.to_categorical(y_train, num_classes)

        self.config["numSensors"] = num_sensors
        self.config["shape"] = input_shape

        return x_train, y_train

    def transformData(self, data, labels, validationData=None):
        self.data = data
        x_train, y_train = self.segRawData(labels)
        if (validationData == None):
            return x_train, y_train

        self.data = validationData
        x_validate, y_validate = self.segRawData(labels)
        return x_train, y_train, x_validate, y_validate

    def segRawData(self, labels):
        epochSets = []
        for dataSet, metaData in self.data:
            try:
                epochs = self.prepareDataSet(labels, dataSet)
                epochSets.append((epochs, metaData))
            except ValueError:
                self.logWarning("whole epoche was dropped due to not existing stage")
                pass

        xArray, yArray = self.segEpochs(epochSets, labels)
        x_train, y_train = self.transform4Keras(xArray, yArray)
        return x_train, y_train

    def main(self):
        labels = self.project.classesMap
        self.data = self.project.getData("trainingRaw", BaseRaw)
        x_train, y_train = self.segRawData(labels)
        self.project.registerData("trainingTransformed", (x_train, y_train))

        self.data = self.project.getData("validationRaw", BaseRaw)
        x_validate, y_validate = self.segRawData(labels)

        self.data = self.project.getData("evaluationRaw", BaseRaw)
        x_validate, y_validate = self.segRawData(labels)

        self.project.registerData("validationTransformed", (x_validate, y_validate))
        self.project.registerData("testTransformed", (x_validate, y_validate))
        # return x_train, y_train, x_validate, y_validate
        self.logWarning("Dropped Epochs because of equalization: %s"%(self.droppedEpochs))


