from pyPhases import Phase
import os
import keras
import numpy as np
import pandas
import seaborn as sns

from keras.models import load_model, Sequential
from keras.layers import Dense, Dropout, Flatten, Reshape, GlobalAveragePooling1D
from keras.layers import MaxPooling2D, Conv1D, MaxPooling1D
from keras.utils import np_utils
from matplotlib import pyplot as plt
from sklearn import metrics
from sklearn.metrics import classification_report


class SleepPhaseDetectionModelEval(Phase):
    """Create Model for sleep stage recognition

    on execution the method main is called
    you can use the phase attributes for better artifact handling
    self.config all configuration input should be stored here (p.e. self.config['epochLength'] = 30.)
    self.metrics the evaluated metrics like 'loss' or 'accuracy' (p.e. self.metrics['accuracy'] = 0.9)
    self.summary
    """

    exportData = []

    def exportLearningCurve(self, history):
        plt.figure(figsize=(6, 4))
        plt.plot(history['accuracy'], "g--", label="Accuracy of training data")
        plt.plot(history['val_accuracy'],
                 "g",
                 label="Accuracy of validation data")
        plt.plot(history['loss'], "r--", label="Loss of training data")
        plt.plot(history['val_loss'], "r", label="Loss of validation data")
        plt.title('Model Accuracy and Loss')
        plt.ylabel('Accuracy and Loss')
        plt.xlabel('Training Epoch')
        plt.ylim(0)
        plt.legend()
        plt.savefig("dist/learningcurve.svg")

    def exportConfusionMatrix(self, validations, predictions):
        matrix = metrics.confusion_matrix(validations, predictions)
        plt.figure(figsize=(6, 4))
        sns.heatmap(matrix,
                    cmap="coolwarm",
                    linecolor='white',
                    linewidths=1,
                    xticklabels=self.labels,
                    yticklabels=self.labels,
                    annot=True,
                    fmt="d")
        plt.title("Confusion Matrix")
        plt.ylabel("True Label")
        plt.xlabel("Predicted Label")
        plt.savefig("dist/confusionmatrix.svg")

    def exportClassificationReport(self, max_y_test, max_y_pred_test):
        report = classification_report(max_y_test,
                                       max_y_pred_test,
                                       output_dict=True)
        df = pandas.DataFrame(report).transpose()
        df.to_html("dist/classification_report.html")

    def main(self):
        project = self.project
        self.model = project.getData('model', keras.Model)
        self.labels = project.classesMap

        x_test, y_test = project.getData('testTransformed', np.ndarray)

        score = self.model.evaluate(x_test, y_test, verbose=0)
        self.metrics["loss"] = score[0]
        self.metrics["accuracy"] = score[1]

        y_pred_test = self.model.predict(x_test)
        # Take the class with the highest probability from the test predictions
        max_y_pred_test = np.argmax(y_pred_test, axis=1)
        max_y_test = np.argmax(y_test, axis=1)

        self.exportConfusionMatrix(max_y_test, max_y_pred_test)
        self.exportClassificationReport(max_y_test, max_y_pred_test)

        return self.model
