from pyPhases import Phase
from .data.EDFData import EDFData
import re


class GetEdfDataForML(Phase):
    """get EDF Data and split them into training/validation/evaluation set

    on execution the method main is called
    you can use the phase attributes for better artifact handling
    self.config all configuration input should be stored here (p.e. self.config['epochLength'] = 30.)
    self.metrics the evaluated metrics like 'loss' or 'accuracy' (p.e. self.metrics['accuracy'] = 0.9)
    self.summary
    """

    exportData = [
        "trainingRaw",
        "validationRaw",
        "evaluationRaw",
    ]

    def main(self):
        training = EDFData(
            "data/",
            "https://physionet.org/files/sleep-edfx/1.0.0/sleep-telemetry/")

        evaluating = EDFData(
            "data/",
            "https://physionet.org/files/sleep-edfx/1.0.0/sleep-telemetry/")

        validation = EDFData(
            "data/",
            "https://physionet.org/files/sleep-edfx/1.0.0/sleep-telemetry/")

        def getMetaData(edfData : EDFData):
            genders = ["F", "M"]
            maxAge = 120

            # patientId = edfData.rawMetaData.patient.decode("utf-8").strip()
            # p = re.compile('X (.) X .*?_(.*)yr')
            # match = p.match(patientId)
            # genderChar = match.group(1)
            # age = int(match.group(2))

            # genderIndex = genders.index(genderChar)
            # ageNormalized = age / maxAge

            return {
                "age": 0,
                "gender": 0
                # "age": ageNormalized,
                # "gender": genderIndex
            }


        training.getMetaDataCallable = getMetaData
        validation.getMetaDataCallable = getMetaData
        evaluating.getMetaDataCallable = getMetaData
        # train data composition

        training.fromUrl("ST7011J0-PSG.edf", "ST7011JP-Hypnogram.edf")

        training.fromUrl("ST7012J0-PSG.edf", "ST7012JP-Hypnogram.edf")
        training.fromUrl("ST7021J0-PSG.edf", "ST7021JM-Hypnogram.edf")
        training.fromUrl("ST7022J0-PSG.edf", "ST7022JM-Hypnogram.edf")
        training.fromUrl("ST7041J0-PSG.edf", "ST7041JO-Hypnogram.edf")
        training.fromUrl("ST7042J0-PSG.edf", "ST7042JO-Hypnogram.edf")
        training.fromUrl("ST7051J0-PSG.edf", "ST7051JA-Hypnogram.edf")
        training.fromUrl("ST7052J0-PSG.edf", "ST7052JA-Hypnogram.edf")
        training.fromUrl("ST7061J0-PSG.edf", "ST7061JR-Hypnogram.edf")
        training.fromUrl("ST7062J0-PSG.edf", "ST7062JR-Hypnogram.edf")
        training.fromUrl("ST7071J0-PSG.edf", "ST7071JA-Hypnogram.edf")
        training.fromUrl("ST7072J0-PSG.edf", "ST7072JA-Hypnogram.edf")
        training.fromUrl("ST7081J0-PSG.edf", "ST7081JW-Hypnogram.edf")
        training.fromUrl("ST7082J0-PSG.edf", "ST7082JW-Hypnogram.edf")
        training.fromUrl("ST7091J0-PSG.edf", "ST7091JE-Hypnogram.edf")
        training.fromUrl("ST7092J0-PSG.edf", "ST7092JE-Hypnogram.edf")
        training.fromUrl("ST7101J0-PSG.edf", "ST7101JE-Hypnogram.edf")
        training.fromUrl("ST7102J0-PSG.edf", "ST7102JE-Hypnogram.edf")
        training.fromUrl("ST7111J0-PSG.edf", "ST7111JE-Hypnogram.edf")
        training.fromUrl("ST7112J0-PSG.edf", "ST7112JE-Hypnogram.edf")
        training.fromUrl("ST7121J0-PSG.edf", "ST7121JE-Hypnogram.edf")
        training.fromUrl("ST7122J0-PSG.edf", "ST7122JE-Hypnogram.edf")
        training.fromUrl("ST7131J0-PSG.edf", "ST7131JR-Hypnogram.edf")
        training.fromUrl("ST7132J0-PSG.edf", "ST7132JR-Hypnogram.edf")
        training.fromUrl("ST7141J0-PSG.edf", "ST7141JE-Hypnogram.edf")
        training.fromUrl("ST7142J0-PSG.edf", "ST7142JE-Hypnogram.edf")
        training.fromUrl("ST7151J0-PSG.edf", "ST7151JA-Hypnogram.edf")
        training.fromUrl("ST7152J0-PSG.edf", "ST7152JA-Hypnogram.edf")
        training.fromUrl("ST7161J0-PSG.edf", "ST7161JM-Hypnogram.edf")
        training.fromUrl("ST7162J0-PSG.edf", "ST7162JM-Hypnogram.edf")
        training.fromUrl("ST7171J0-PSG.edf", "ST7171JA-Hypnogram.edf")
        training.fromUrl("ST7172J0-PSG.edf", "ST7172JA-Hypnogram.edf")
        training.fromUrl("ST7181J0-PSG.edf", "ST7181JR-Hypnogram.edf")
        training.fromUrl("ST7182J0-PSG.edf", "ST7182JR-Hypnogram.edf")
        training.fromUrl("ST7191J0-PSG.edf", "ST7191JR-Hypnogram.edf")
        training.fromUrl("ST7192J0-PSG.edf", "ST7192JR-Hypnogram.edf")
        trainDataRaw = training.getData("training")

        # # evaluate the model
        validation.fromUrl("ST7241J0-PSG.edf", "ST7241JO-Hypnogram.edf")
        validation.fromUrl("ST7222J0-PSG.edf", "ST7222JA-Hypnogram.edf")
        validation.fromUrl("ST7242J0-PSG.edf", "ST7242JO-Hypnogram.edf")
        validation.fromUrl("ST7211J0-PSG.edf", "ST7211JJ-Hypnogram.edf")
        validationDataRaw = validation.getData("validating")

        # # evaluate the model
        evaluating.fromUrl("ST7201J0-PSG.edf", "ST7201JO-Hypnogram.edf")
        evaluating.fromUrl("ST7212J0-PSG.edf", "ST7212JJ-Hypnogram.edf")
        evaluating.fromUrl("ST7221J0-PSG.edf", "ST7221JA-Hypnogram.edf")
        evaluating.fromUrl("ST7202J0-PSG.edf", "ST7202JO-Hypnogram.edf")
        testDataRaw = evaluating.getData("evaluation")

        self.project.registerData("trainingRaw", trainDataRaw)
        self.project.registerData("validationRaw", validationDataRaw)
        self.project.registerData("evaluationRaw", testDataRaw)
