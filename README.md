# sleepClassificationCNN

CNN Model to recognize sleep stages based on [A Deep Learning Model for Automated Sleep Stages Classification Using PSG Signals](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6406978/) from Ozal Yildirim, Ulas Baran Baloglu and U Rajendra Acharya.

# setup

`pip install -r requirements.txt`

# train the model
## complete

`python sleepClassificationCNN`

## single stages

### gather and prepare data

`python sleepClassificationCNN prepareData`

### train

`python sleepClassificationCNN train`

### evaluate

`python sleepClassificationCNN evaluate`
