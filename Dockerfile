# FROM tensorflow/tensorflow:2.1.0-py3 as prod

# EXPOSE 5000
# ENV VERSION="0.0.0"
# WORKDIR /app

# COPY requirements.txt .
# RUN apt-get -y update && apt-get install -y \
#         graphviz \
#         git \
#     && pip install -r requirements.txt

# COPY sleepClassificationCNN .

# ENTRYPOINT ["python", "."]

FROM rocm/tensorflow:rocm3.1-tf2.1-dev as rocm

EXPOSE 5000
ENV VERSION="0.0.0"
WORKDIR /app

COPY sleepClassificationCNN .

RUN add-apt-repository -y ppa:deadsnakes/ppa && \
    apt-get -y update && apt-get install -y \
        rocm-libs \
        hipcub \
        miopen-hip \
        graphviz \
        git \
        python3.7 \
        python3.7-distutils && \
    apt-get remove -y python3-pip && \
    python3.7 -m easy_install pip \
    && apt-get remove -y python-setuptools && \
    python3.7 -m pip install -U pip setuptools

COPY requirements.txt .

RUN python3.7 -m pip install --user tensorflow-rocm --upgrade && \
    usermod -a -G video root && \
    python3.7 -m pip install -r requirements.txt

ENTRYPOINT ["python3.7", "."]
